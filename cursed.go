package cursed

//#include <locale.h>
//#include <stdlib.h>
//struct ldat{};
//struct _win_st{};
//#define _Bool int
//#define NCURSES_OPAQUE 1
//#define _XOPEN_SOURCE_EXTENDED 1
//#include <ncurses.h>
import "C"

import (
    "os"
	"utf16"
)

type Window C.WINDOW
type Chtype C.chtype

type CursesError struct {
    message string
}

func (ce CursesError) String() string {
    return ce.message
}

// Cursor options.
const (
    CURS_HIDE = iota
    CURS_NORM
    CURS_HIGH
)

// Pointers to the values in curses, which may change values.
// TODO: Replace these ugly accessors
//var Cols *int = nil;
//var Rows *int = nil;
//
//var Colors *int = nil;
//var ColorPairs *int = nil;
//
//var Tabsize *int = nil;
// ENDTODO

const (
	ERR = C.ERR
	OK = C.OK
	A_ALTCHARSET = C.A_ALTCHARSET
	A_BLINK = C.A_BLINK
	A_BOLD = C.A_BOLD
	A_DIM = C.A_DIM
	A_INVIS = C.A_INVIS
	A_PROTECT = C.A_PROTECT
	A_REVERSE = C.A_REVERSE
	A_STANDOUT = C.A_STANDOUT
	A_UNDERLINE = C.A_UNDERLINE
	A_ATTRIBUTES = C.A_ATTRIBUTES
	A_CHARTEXT = C.A_CHARTEXT
	A_COLOR = C.A_COLOR
	WA_ALTCHARSET = C.WA_ALTCHARSET
	WA_BLINK = C.WA_BLINK
	WA_BOLD = C.WA_BOLD
	WA_DIM = C.WA_DIM
	WA_INVIS = C.WA_INVIS
	WA_LEFT = C.WA_LEFT
	WA_PROTECT = C.WA_PROTECT
	WA_REVERSE = C.WA_REVERSE
	WA_RIGHT = C.WA_RIGHT
	WA_STANDOUT = C.WA_STANDOUT
	WA_UNDERLINE = C.WA_UNDERLINE
	COLOR_BLACK = C.COLOR_BLACK
	COLOR_BLUE = C.COLOR_BLUE
	COLOR_GREEN = C.COLOR_GREEN
	COLOR_CYAN = C.COLOR_CYAN
	COLOR_RED = C.COLOR_RED
	COLOR_MAGENTA = C.COLOR_MAGENTA
	COLOR_YELLOW = C.COLOR_YELLOW
	COLOR_WHITE = C.COLOR_WHITE
	KEY_BREAK = C.KEY_BREAK
	KEY_DOWN = C.KEY_DOWN
	KEY_UP = C.KEY_UP
	KEY_LEFT = C.KEY_LEFT
	KEY_RIGHT = C.KEY_RIGHT
	KEY_HOME = C.KEY_HOME
	KEY_BACKSPACE = C.KEY_BACKSPACE
	KEY_F0 = C.KEY_F0
	KEY_DL = C.KEY_DL
	KEY_IL = C.KEY_IL
	KEY_DC = C.KEY_DC
	KEY_IC = C.KEY_IC
	KEY_EIC = C.KEY_EIC
	KEY_CLEAR = C.KEY_CLEAR
	KEY_EOS = C.KEY_EOS
	KEY_EOL = C.KEY_EOL
	KEY_SF = C.KEY_SF
	KEY_SR = C.KEY_SR
	KEY_NPAGE = C.KEY_NPAGE
	KEY_PPAGE = C.KEY_PPAGE
	KEY_STAB = C.KEY_STAB
	KEY_CTAB = C.KEY_CTAB
	KEY_CATAB = C.KEY_CATAB
	KEY_ENTER = C.KEY_ENTER
	KEY_SRESET = C.KEY_SRESET
	KEY_RESET = C.KEY_RESET
	KEY_PRINT = C.KEY_PRINT
	KEY_LL = C.KEY_LL
	KEY_A1 = C.KEY_A1
	KEY_A3 = C.KEY_A3
	KEY_B2 = C.KEY_B2
	KEY_C1 = C.KEY_C1
	KEY_C3 = C.KEY_C3
	KEY_BTAB = C.KEY_BTAB
	KEY_BEG = C.KEY_BEG
	KEY_CANCEL = C.KEY_CANCEL
	KEY_CLOSE = C.KEY_CLOSE
	KEY_COMMAND = C.KEY_COMMAND
	KEY_COPY = C.KEY_COPY
	KEY_CREATE = C.KEY_CREATE
	KEY_END = C.KEY_END
	KEY_EXIT = C.KEY_EXIT
	KEY_FIND = C.KEY_FIND
	KEY_HELP = C.KEY_HELP
	KEY_MARK = C.KEY_MARK
	KEY_MESSAGE = C.KEY_MESSAGE
	KEY_MOVE = C.KEY_MOVE
	KEY_NEXT = C.KEY_NEXT
	KEY_OPEN = C.KEY_OPEN
	KEY_OPTIONS = C.KEY_OPTIONS
	KEY_PREVIOUS = C.KEY_PREVIOUS
	KEY_REDO = C.KEY_REDO
	KEY_REFERENCE = C.KEY_REFERENCE
	KEY_REFRESH = C.KEY_REFRESH
	KEY_REPLACE = C.KEY_REPLACE
	KEY_RESTART = C.KEY_RESTART
	KEY_RESUME = C.KEY_RESUME
	KEY_SAVE = C.KEY_SAVE
	KEY_SBEG = C.KEY_SBEG
	KEY_SCANCEL = C.KEY_SCANCEL
	KEY_SCOMMAND = C.KEY_SCOMMAND
	KEY_SCOPY = C.KEY_SCOPY
	KEY_SCREATE = C.KEY_SCREATE
	KEY_SDC = C.KEY_SDC
	KEY_SDL = C.KEY_SDL
	KEY_SELECT = C.KEY_SELECT
	KEY_SEND = C.KEY_SEND
	KEY_SEOL = C.KEY_SEOL
	KEY_SEXIT = C.KEY_SEXIT
	KEY_SFIND = C.KEY_SFIND
	KEY_SHELP = C.KEY_SHELP
	KEY_SHOME = C.KEY_SHOME
	KEY_SIC = C.KEY_SIC
	KEY_SLEFT = C.KEY_SLEFT
	KEY_SMESSAGE = C.KEY_SMESSAGE
	KEY_SMOVE = C.KEY_SMOVE
	KEY_SNEXT = C.KEY_SNEXT
	KEY_SOPTIONS = C.KEY_SOPTIONS
	KEY_SPREVIOUS = C.KEY_SPREVIOUS
	KEY_SPRINT = C.KEY_SPRINT
	KEY_SREDO = C.KEY_SREDO
	KEY_SREPLACE = C.KEY_SREPLACE
	KEY_SRIGHT = C.KEY_SRIGHT
	KEY_SRSUME = C.KEY_SRSUME
	KEY_SSAVE = C.KEY_SSAVE
	KEY_SSUSPEND = C.KEY_SSUSPEND
	KEY_SUNDO = C.KEY_SUNDO
	KEY_SUSPEND = C.KEY_SUSPEND
	KEY_UNDO = C.KEY_UNDO
)

// Definitions for printed characters not found on most keyboards. Ideally,
// these would not be hard-coded as they are potentially different on
// different systems. However, some ncurses implementations seem to be
// heavily reliant on macros which prevent these definitions from being
// handled by cgo properly. If they don't work for you, you won't be able
// to use them until either a) the Go team works out a way to overcome this
// limitation in godefs/cgo or b) an alternative method is found. Work is
// being done to find a solution from the ncurses source code.
const (
        ACS_DEGREE = iota + 4194406
        ACS_PLMINUS
        ACS_BOARD
        ACS_LANTERN
        ACS_LRCORNER
        ACS_URCORNER
        ACS_LLCORNER
        ACS_ULCORNER
        ACS_PLUS
        ACS_S1
        ACS_S3
        ACS_HLINE
        ACS_S7
        ACS_S9
        ACS_LTEE
        ACS_RTEE
        ACS_BTEE
        ACS_TTEE
        ACS_VLINE
        ACS_LEQUAL
        ACS_GEQUAL
        ACS_PI
        ACS_NEQUAL
        ACS_STERLING
        ACS_BULLET
        ACS_LARROW  = 4194347
        ACS_RARROW  = 4194348
        ACS_DARROW  = 4194349
        ACS_UARROW  = 4194350
        ACS_BLOCK   = 4194352
        ACS_DIAMOND = 4194400
        ACS_CKBOARD = 4194401
)

func boolToInt(b bool) C.int {
    if b {
        return C.TRUE
    }
    return C.FALSE
}

func intToBool(b C.int) bool {
    if b == C.TRUE {
        return true
    }
    return false
}

func isOk(ok C.int) bool {
    if ok == C.OK {
        return true
    }
    return false
}

func Init() (*Window, os.Error) {
	C.setlocale(C.LC_ALL, C.CString(""));
	stdwin := (*Window)(C.initscr());

	if stdwin == nil {
		return nil, CursesError{"Init failed"};
	}

	return stdwin, nil;
}

func NewWin(rows int, cols int, starty int, startx int) (*Window, os.Error) {
	nw := (*Window)(C.newwin(C.int(rows), C.int(cols), C.int(starty), C.int(startx)));

	if nw == nil {
		return nil, CursesError{"Failed to create window"};
	}

	return nw, nil;
}

func (win *Window) Del() os.Error {
	if int(C.delwin(win.AsCPtr())) == 0 {
		return CursesError{"*Window.Del() failed"};
	}
	return nil
}

func (win *Window) SubWin(rows int, cols int, starty int, startx int) (*Window, os.Error)  {
	sw := (*Window)(C.subwin(win.AsCPtr(), C.int(rows), C.int(cols), C.int(starty), C.int(startx)));

	if sw == nil {
		return nil, CursesError{"*Window.SubWin() failed"};
	}

	return sw, nil;
}

func (win *Window) DerWin(rows int, cols int, starty int, startx int) (*Window, os.Error)  {
	dw := (*Window)(C.derwin(win.AsCPtr(), C.int(rows), C.int(cols), C.int(starty), C.int(startx)));

	if dw == nil {
		return nil, CursesError{"*Window.DerWin() failed"};
	}

	return dw, nil;
}

// COLORS! \o/
var HasColors bool = false; // Whether color options are null ops.

func TryColors() bool {
	err := RequireColors();
	if err != nil {
		return false;
	}
	return true;
}

func RequireColors() os.Error {
	HasColors = false;

	if int(C.has_colors()) != 1 {
		return CursesError{"Terminal does not support color"};
	}
	if int(C.start_color()) != int(C.OK) {
		return CursesError{"Curses failed to start_color()"};
	}

	if int(C.can_change_color()) != int(C.OK) {
		return CursesError{"Terminal does not support redefining colors"};
	}

	if int(C.use_default_colors()) != int(C.OK) {
		return CursesError{"Terminal does not support default colors"};
	}

	if int(C.assume_default_colors(-1, -1)) != int(C.OK) {
		return CursesError{"Terminal does not support default colors for color pair 0"};
	}

	HasColors = true;
	return nil;
}


var colorCounter int = 1; // Incrementer for auto-added colorpairs

func AddColorPair(fg int, bg int) (int, os.Error) {
	i := colorCounter;
	var e os.Error = nil;
	if !HasColors {
		return i, e;
	}

	if colorCounter >= int(C.COLOR_PAIRS) {
		e = CursesError{"New color id exceeds max colors."};
		return i, e;
	}

	if C.init_pair(C.short(colorCounter), C.short(fg), C.short(bg)) != C.OK {
		e = CursesError{"C.init_pair() failed."};
		return i, e;
	}

	colorCounter += 1;
	return i, e;
}

func ColorPair(id int) int {
	return int(C.COLOR_PAIR(C.int(id)));
}

func NoEcho() os.Error {
	if int(C.noecho()) != int(C.OK) {
		return CursesError{"NoEcho failed"};
	}
	return nil;
}

func DoUpdate() os.Error {
	if int(C.doupdate()) != int(C.OK) {
		return CursesError{"DoUpdate failed"};
	}
	return nil;
}

func Echo() os.Error {
	if int(C.noecho()) != int(C.OK) {
		return CursesError{"Echo failed"};
	}
	return nil;
}

func CursSet(c int) os.Error {
	if int(C.curs_set(C.int(c))) != int(C.OK) {
		return CursesError{"CursSet failed"};
	}
	return nil;
}

func NoCBreak() os.Error {
	if int(C.nocbreak()) != int(C.OK) {
		return CursesError{"NoCBreak failed"};
	}
	return nil;
}

func CBreak() os.Error {
	if int(C.cbreak()) != int(C.OK) {
		return CursesError{"CBreak failed"};
	}
	return nil;
}

func EndWin() os.Error {
	if int(C.endwin()) != int(C.OK) {
		return CursesError{"EndWin failed"};
	}
	return nil;
}

func (win *Window) GetCh() int {
	return int(C.wgetch(win.AsCPtr()));
}

// StringToUTF16 returns the UTF-16 encoding of the UTF-8 string s,
// with a terminating NUL added.
func stringToWCharPtr(s string) (*C.wchar_t, uint) {
	var wstr []C.wchar_t;
	var total uint = 0;
	// convert to utf-16, then []wchar_t
	for _, ch := range utf16.Encode([]int(s+"\x00")) {
		wstr = append(wstr, C.wchar_t(ch));
		total += 1;
	}
	return &wstr[0], total;
}

func (win *Window) AddStr(s string) {
	/*
	// Convert string to []wchar_t
	var wstr []C.wchar_t;
	for _, ch := range utf16.Encode([]int(s+"\x00")) {
		wstr = append(wstr, C.wchar_t(ch));
	}

	C.waddwstr(win.AsCPtr(), &wstr[0]);
	*/
	wstr, _ := stringToWCharPtr(s);
	C.waddwstr(win.AsCPtr(), wstr);
}

func (win *Window) AddStrAt(s string, x, y int) {
	x2 := C.int(x);
	y2 := C.int(y);
	wstr, _ := stringToWCharPtr(s);
	C.mvwaddwstr(win.AsCPtr(), y2, x2, wstr);
}

func (win *Window) AddStrAttr(s string, attrs ...int) {
	win.AddStrColorAttr(s, 0, attrs...);
}

func (win *Window) AddStrAtAttr(s string, x, y int, attrs ...int) {
	win.Move(x, y);
	win.AddStrColorAttr(s, 0, attrs...);
}

func (win *Window) AddStrColor(s string, colpair_id int) {
	win.AddStrColorAttr(s, colpair_id);
}

func (win *Window) AddStrColorAttr(s string, colpair_id int, attrs ...int) {
	var newattr int = 0;
    for _, attr := range attrs {
        newattr = newattr | attr;
    }

	attribs := newattr|ColorPair(colpair_id);
	wstr, _ := stringToWCharPtr(s);
	C.attron(C.int(attribs));
	C.waddwstr(win.AsCPtr(), wstr);
	C.attroff(C.int(attribs));
}

func (win *Window) AddStrColorAt(s string, colpair_id, x, y int) {
	win.Move(x, y);
	win.AddStrColorAttr(s, colpair_id);
}

func (win *Window) AddStrColorAtAttr(s string, colpair_id, x, y int, attrs ...int) {
	win.Move(x, y);
	win.AddStrColorAttr(s, colpair_id, attrs...);
}

// Normally Y is the first parameter passed in curses.
func (win *Window) Move(x, y int) {
	C.wmove(win.AsCPtr(), C.int(y), C.int(x));
}

func (w *Window) Keypad(tf bool) os.Error {
	var outint int;
	if tf == true {outint = 1;}
	if tf == false {outint = 0;}
	if C.keypad(w.AsCPtr(), C.int(outint)) == 0 {
		return CursesError{"Keypad failed"};
	}
	return nil;
}

func (win *Window) Refresh() os.Error {
	if C.wrefresh(win.AsCPtr()) == 0 {
		return CursesError{"refresh failed"};
	}
	return nil;
}

func (win *Window) Redrawln(beg_line, num_lines int) {
	C.wredrawln(win.AsCPtr(), C.int(beg_line), C.int(num_lines));
}

func (win *Window) Redraw() {
	C.redrawwin(win.AsCPtr());
}

func (win *Window) Clear() {
	C.wclear(win.AsCPtr());
}

func (win *Window) Erase() {
	C.werase(win.AsCPtr());
}

func (win *Window) Clrtobot() {
	C.wclrtobot(win.AsCPtr());
}

func (win *Window) Clrtoeol() {
	C.wclrtoeol(win.AsCPtr());
}

func (win *Window) Box(verch, horch int) {
	C.box(win.AsCPtr(), C.chtype(verch), C.chtype(horch));
}

func (win *Window) Background(colour int32) {
	C.wbkgd(win.AsCPtr(), C.chtype(colour));
}

func (win *Window) AsCPtr() *C.WINDOW {
	return (*C.WINDOW)(win);
}
