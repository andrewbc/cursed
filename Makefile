include $(GOROOT)/src/Make.inc

TARG=cursed
CGOFILES=\
	cursed.go\
	menus.go\
	forms.go\
	panels.go\

CGO_LDFLAGS=-lmenu -lform -lpanel -lncursesw

include $(GOROOT)/src/Make.pkg
