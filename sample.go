package main

import (
	. "cursed"
	"os"
	"fmt"
)

func main() {
	win, err := Init();
	defer EndWin();
	if err != nil {
		fmt.Printf("There was an error starting cursed: %s\n", err);
		os.Exit(1);
	}


	//TryColors();
	// TryColors() => bool
	// This  attempts to init color and color based extensions to ncurses.
	// If it's unable, color related functions are null operations and default
	// text is used for everything.
	// Check cursed.HasColors or the return value for whether you have color.
	//
	// returns => bool
	// If you NEED colors, use cursed.RequireColors() => os.Error
	if e := RequireColors(); e != nil {
		fmt.Printf("Color error: %s", e);
		os.Exit(1);
	}

	c_red, _ := AddColorPair(COLOR_RED, COLOR_BLACK);
	c_green, _ := AddColorPair(COLOR_BLUE, COLOR_WHITE);
	c_blue, _ := AddColorPair(COLOR_GREEN, -1);

	NoEcho();
	CursSet(CURS_HIDE);
	// Most of the ported function names have a SaneNamingScheme

	win.Keypad(true);

	x, y := 5, 10;
	for {
		win.Clear();
		win.AddStr("Hello, world!\n");
		win.AddStrColor("or Καλημέρα κόσμε\n", c_red);
		win.AddStrColor("or Tag, Welt!\n", c_green);
		win.AddStrColor("or こんにちは 世界\n", c_blue);
		win.Refresh();

		inp := win.GetCh();
		if inp == 'q' {
			// Quit if the user presses q
			break;
		}
		if inp == KEY_LEFT {
			x = x - 1;
		}
		if inp == KEY_RIGHT {
			x = x + 1;
		}
		if inp == KEY_UP {
			y = y - 1;
		}
		if inp == KEY_DOWN {
			y = y + 1;
		}
	}
}
