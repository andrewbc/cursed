Cursed
======
Cursed is a non-conformant Go binding of ncurses that attempts to make working
with curses easier and simpler.

Goals of Cursed
---------------
Cursed supports utf-8 by default, making the integration with Go seamless.
It also supports 256 colors, or no colors. You can require colors, or keep it
optional. Overall, Cursed tries to ignore the complexity of the many variants 
of ncurses to what's necessary to get the job done.

License
-------
Cursed is released under the simplified 2-clause BSD license (AKA The FreeBSD
License) You can find the license text in LICENSE in the base directory of
the repository.
